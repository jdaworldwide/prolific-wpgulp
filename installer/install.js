#!/usr/bin/env node

const fs = require('fs'); // node file system
const ora = require('ora'); // elegant terminal spinner
const execa = require('execa'); // process execution for humans
const {yellow: y, green: g, dim: d} = require('chalk'); // terminal string highlighting done right
const download = require('download'); // download and extract files
const clear = require('clear-any-console'); // cross platform console clear for Node.js CLI

const errorHandler = require('./errorHandler');
const nextSteps = require('./nextSteps');

const spinner = ora({text: ''});

(async () => {
    clear(); // Clear the current console window

    // Get the current working directory (ex: /Users/user/my-project)
    // Split it into an array (ex: ['', 'Users', 'user', 'my-project'])
    // And grab the last item in the array to use as an install directory
    const CWD = process.cwd();
    const CWDArray = CWD.split('/');
    const installDir = CWDArray[CWDArray.length - 1];

    // Files to download
    const filesToDownload = [
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/package.json',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/gulpfile.js',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/.gitignore',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/README.md',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/functions.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/header.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/index.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/footer.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/style.css',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/screenshot.jpg',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/init.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/composer.json',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/components/blocks.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/components/cpts.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/acf.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/gravityforms.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/duplicate-post.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/what-the-file.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/wp-migrate-db-pro.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/wp-migrate-db-pro-media-files.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/acf-extended-pro.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins/user-switching.zip',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/dependencies.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/disable-comments.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/filters.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/fix-gravity-forms.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/helpers.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/lib/required-plugins.php',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/sass/admin.scss',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/sass/blocks.scss',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/sass/editor.scss',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/sass/print.scss',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/sass/style.scss',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/src/js/template.js',
        'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/template-parts/blocks/wysiwyg_editor.php'
    ];

    // This was removed from the above array
        //'https://bitbucket.org/jdaworldwide/prolific-wpgulp/raw/master/app/inc/composer.lock',

    // Dotfiles
    const dotFiles = ['.gitignore'];

    // Start doing all the things
    console.log();
    console.log(g('Installing Prolific-WPGulp in directory:'), d(installDir));
    console.log(d('Be patient. Pay no attention to the man behind the curtain.'));
    console.log();

    // Start our spinner
    spinner.start(`${y('DOWNLOADING')} Prolific-WPGulp files...`);

    // Get the things
    Promise.all(filesToDownload.map(x => download(x, `${CWD}`))).then(async () => {
        dotFiles.map(dotFile =>
            fs.rename(
                `${CWD}/${dotFile.slice(1)}`, // e.g. gitignore without a (.) prefix.
                `${CWD}/${dotFile}`, // Add the (.) preferred
                err => errorHandler(err)
            )
        );

        spinner.succeed(`${g('DOWNLOADED')} Prolific-WPGulp files`);

        spinner.start(`${g('CREATING')} project directory structure`);

        // Make our directories
        await execa('mkdir', ['inc', 'src', 'template-parts']); // root directories
        await execa('mkdir', ['inc/lib', 'inc/components', 'inc/lib/required-plugins']); // inc director
        await execa('mkdir', ['src/copy', 'src/img', 'src/sass', 'src/sass/abstracts', 'src/sass/base', 'src/sass/blocks', 'src/sass/layouts', 'src/sass/other', 'src/js', 'src/js/admin', 'src/js/frontend', 'src/js/admin/modules', 'src/js/frontend/modules']);
        await execa('mkdir', ['template-parts/blocks']);

        // Move files to directores
        // await execa('mv', ['init.php', 'composer.json', 'composer.lock', 'inc']);
        await execa('mv', ['init.php', 'composer.json', 'inc']);
        await execa('mv', ['blocks.php', 'cpts.php', 'inc/components']);
        await execa('mv', ['acf.zip', 'acf-extended-pro.zip', 'user-switching.zip', 'gravityforms.zip', 'duplicate-post.zip', 'what-the-file.zip', 'wp-migrate-db-pro.zip', 'wp-migrate-db-pro-media-files.zip', 'inc/lib/required-plugins']);
        await execa('mv', ['dependencies.php', 'disable-comments.php', 'filters.php', 'fix-gravity-forms.php', 'helpers.php', 'required-plugins.php', 'inc/lib']);
        await execa('mv', ['admin.scss', 'blocks.scss', 'editor.scss', 'print.scss', 'style.scss', 'src/sass']);
        await execa('cp', ['template.js', 'src/js/admin/admin.js']);
        await execa('cp', ['template.js', 'src/js/frontend/app.js']);
        await execa('rm', ['template.js']);
        await execa('mv', ['wysiwyg_editor.php', 'template-parts/blocks']);

        // Make dummy files
        await execa('touch', ['src/sass/abstracts/_functions.scss', 'src/sass/abstracts/_mixins.scss', 'src/sass/abstracts/_variables.scss'] );
        await execa('touch', ['src/sass/base/_buttons.scss', 'src/sass/base/_typography.scss', 'src/sass/base/_utilities.scss'] );
        await execa('touch', ['src/sass/blocks/_wysiwyg.scss'] );
        await execa('touch', ['src/sass/layouts/_archive.scss', 'src/sass/layouts/_footer.scss', 'src/sass/layouts/_header.scss', 'src/sass/layouts/_home.scss', 'src/sass/layouts/_search.scss'] );
        await execa('touch', ['src/sass/other/_admin.scss', 'src/sass/other/_editor.scss'] );

        spinner.succeed(`${g(`CREATED`)} WordPress directory structure`);

        spinner.start(`${y(`INSTALLING`)} npm and composer packages…`);
        await execa(`npm`, [`install`]);
        spinner.succeed(`${g(`INSTALLED`)} npm and composer packages`);

        nextSteps();
    });
})();