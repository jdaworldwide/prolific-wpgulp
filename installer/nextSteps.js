const sym = require('log-symbols');
const {green: g, yellow: y, cyan: c, dim: d, white: w,} = require('chalk');

module.exports = () => {
    console.log();
    console.log(sym.success, g(`ALL DONE! You did the thing!`));
    console.log(`Prolific-WPGulp has been installed into the current directory.`);
    console.log(d(`This directory is your new WordPress theme and should be placed inside of a WordPress installation on a running webserver.`));

    console.log();
    console.log(g(`QUICK START`));
    console.log(`1. Ensure that you already have a local webserver running with WordPress installed.`);
    console.log(d(`This implementation of browser-sync creates a proxy to your web server.`));
    console.log();
    console.log(`2. Edit the ${c(`package.json`)} file.`);
    console.log(d(`At minimum, you should edit the name, version and URLs to match your project or the build process will likely fail or produce unexpected results.`));
    console.log();
    console.log(`3. Optionally, install additional dependencies with ${c(`npm install -S package-name`)}.`);
    console.log(d(`For easy reference, anything listed as a dependency will be symlinked inside the`), y(`vendor_libraries`), d(`folder.`));
    console.log(d(`This folder has been ignored in git and is generated each time Gulp starts.`));
    console.log();
    console.log(`${d(`Sass has been configured to include the`)} ${y(`node_modules`)} ${d(`folder.`)}`);
    console.log(d(`You can include Sass dependencies by including the path to its .scss file in your import statement.`));
    console.log(`${d(`Example: `)} ${c(`@import "bourbon/core/_bourbon.scss`)}`);
    console.log();
    console.log(`${d(`To automatically compile JavaScript dependencies, add the full path to the`)} ${c(`paths.src.js.appLibraries`)} ${d(`array inside`)} ${c(`package.json`)}`);
    console.log(d(`Example:`));
    console.log(c(`{...`));
    console.log(c(`  "paths": {`));
    console.log(c(`    "src": {`));
    console.log(c(`      "js": {...`));
    console.log(c(`        "appLibraries": [`));
    console.log(c(`          "./node_modules/jquery/dist/jquery.js",`));
    console.log(c(`          "./node_modules/bootstrap/bootstrap.bundle.js"`));
    console.log(c(`        ]`));
    console.log(c(`      }`));
    console.log(c(`    }`));
    console.log(c(`  }`));
    console.log(c(`}`));

    console.log();
    console.log(`3. Begin building the project with ${g('npm start')}.`);
    console.log(d(`This will start browser-sync and watch for changes.`));
    console.log(d(`Additional commands are documented in the readme file included with this project.`));

    console.log();
    console.log(g(`Thank you for installing Prolific-WPGulp!`));
    console.log(`${d(`Special thanks to`)} ${w(`@AhmadAwais`)} ${d(`for the amazing WPGulp workflow.`)}`);
    console.log(d(`Without it, this project would not have been possible.`));
    console.log(c(`https://github.com/ahmadawais/WPGulp`));
    console.log();
};
