# Changelog

# 0.0.20
- Included package.json in the gulp zip command

# 0.0.19
- I need coffee. Actually put the zip files in the right place.

## 0.0.18
- Actuall added our new plugin files to the downloader during install.js

## 0.0.17
- Added `wp-config.php` setup instructions to README.md

## 0.0.16
- Added ACF Extended to required plugins
- Added User Switching to recommended plugins

## 0.0.15
- Added conditional check to include SVG in header.php to prevent 404's when SVGs are not included.
- Fixed JavaScript enqueing in depenedencies.php to consistently use the THEME_SLUG variable
- Added roadmap.md
- Added changelog.md

## 0.0.14
- Clarified post-install instructions

## 0.0.13
- Added support for ES6 via Babel

## 0.0.12
- Remove test imports from SASS blocks.scss

## 0.0.11
- Fix URLs in package.json

## 0.0.10
- Initial production release.