# prolific-wpgulp

## Overview

This is a combination of our WordPress starter theme and a heavily modified [WPGulp](https://github.com/ahmadawais/WPGulp) workflow maintaned by AhmadAwais.

**NOTE:** This project __requires__ node version 14.0.0! Install this using Node Version Manager

# Prerequisites

This project uses Gulp, a [Node.js](https://nodejs.org/en/download/package-manager/) task runner and should already be installed as a prerequisite.

Confirm that node and npm are properly installed using the command line:

    node --version
    npm --version

Additionally, the Gulp command command line must be globaly installed.

    npm install --global gulp-cli

You can verify that gulp-cli is installed by running

    gulp --version

Finally, portions of this project are built using [Composer](https://getcomposer.org), and `npm postinstall` will fail if Composer is not installed. Verify composer is properly installed with:

    composer --version

# Quickstart: Homebrew on macOS
On macOS, ensure command line tools are installed. If they are not, install them using

    xcode-select --install


Then, install and update [Homebrew](https://brew.sh).
At the time of this writing, past the following into terminal:

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew doctor
    brew upgrade

Next, install node, gulp-cli and composer:

    brew install node gulp-cli composer

# Installing this project

After the prerequisites have been met, navigate to your the `wp-content/themes/` folder and create a new folder to house your new theme. Inside this folder, run `npx prolific-wpgulp`

After complete, make sure to read the new README.md file inside your new theme folder.