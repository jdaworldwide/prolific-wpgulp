'use strict';

// grab our package variables
const pkg = require( './package.json' );

// Load gulp as a standalone variable
const gulp = require( 'gulp' );

// Load gulp plugins prefixed with $
const $ = require( 'gulp-load-plugins' )({
    pattern: ['*'],
    scope: ['devDependencies'],
    rename: {
        'postcss' : 'postcss-core',
        'jshint' : 'jshint-core'
    }
});

// Error handling
const onError = (err) => {
    console.log(err);
};

function getGitString() {
    if ( $.checkGit(process.cwd())) {
        return $.gitRevSync.long() + ' [' + $.gitRevSync.branch() + ']';
    } else {
        return 'No git repository information found';
    }
};

// Our banner
const banner = {
    main:
        '/**\n' +
        ' * @project           <%= pkg.name %>\n' +
        ' * @author            <%= pkg.author %>\n' +
        ' * @version           <%= pkg.version %>\n' +
        ' * @build             ' + $.moment().format('llll') + ' ET\n' +
        ' * @release           ' + getGitString() + '\n' +
        ' * @copyright         Copyright (c) ' + $.moment().format('YYYY') + ' <%= pkg.copyright %>\n' +
        '**/\n'
};

// Clean our build directory
function clean( cb ) {
    $.del.sync([
        pkg.paths.dist.base,
        './vendor_libraries'
    ]);
    cb();
};

// Start browser-sync
function startServer( cb ) {
    $.browserSync.init({
        proxy: pkg.urls.local
    });
    cb();
};

// Reload the browser
function reload( cb ) {
    $.browserSync.reload();
    cb();
};

/*
 * For sanity sake, creatye symlinks to our Dependencies listed in package.json
 * This folder is excluded from git
 */
function linkDependencies( cb) {
    return $.depLinker.linkDependenciesTo( './vendor_libraries' );
    cb();
};

/*
 * Handle CSS
 *
 * This task does the following:
 *   1. Gets our source scss files
 *   2. Compiles Sass to CSS
 *   3. Autoprefixes according to the browser config in package.json
 *   4. Generates our source maps
 *   5. Minifies the stylesheets
 *   6. Generates RTL stylesheets
 *   7. Inject new CSS into browser
 */
function styles( cb ) {
    return gulp
        .src( pkg.paths.src.sass + '**/*.scss' )
        .pipe( $.plumber({errorHandler: onError}) )         // Start our error handler so gulp doesn't break
        .pipe( $.sourcemaps.init() )                        // Sourcemaps, baby
        .pipe(                                              // Do the sass and check node_modules for dependencies
            $.sass({
                outputStyle: 'expanded',
                includePaths: './node_modules/'
            })
            .on('error', $.sass.logError)
        )
        .pipe( $.postcss([$.autoprefixer()]) )              // Autoprefix based off of package.json definitions
        .pipe( $.header(banner.main, {pkg: pkg}) )          // inject our banners to our core CSS files
        .pipe( $.sourcemaps.write('.') )                    // Generate a map file instead of inline'ing it
        .pipe( gulp.dest( pkg.paths.dist.sass ) )           // write our files
        .pipe( $.filter('**/*.css') )                       // Filter stream to only css files
        .pipe( $.browserSync.stream() )                     // Reloads our style.css if its enqueued
        .pipe( $.rename({suffix: '.min'}) )                 // Add .min to the css files
        .pipe( $.postcss([$.cssnano({
            preset: [ 'default', { discardComments: { removeAll: true } } ]
        })]))                   // Minify them
        .pipe( $.header(banner.main, {pkg: pkg}) )          // Write our banner to the minified file
        .pipe( gulp.dest( pkg.paths.dist.sass ) )           // Write the minified files to disk
        .pipe( $.filter('**/*.css') )                       // Filter stream to only css files
        .pipe( $.browserSync.stream() )                     // Reload if .mins.css are enqueued
        .pipe( $.rename({ suffix: '-rtl' }) )               // add an -rtl suffix
        .pipe( $.rtlcss() )                                 // Generate a minified rtl stylesheet
        .pipe( gulp.dest( pkg.paths.dist.sass ) );          // Writes our RTL stylesheet to disk
};

/**
 * Lint all the scripts we wrote and output them to console
 */
function lintScripts( cb ) {
    return gulp
        .src( pkg.paths.src.js.base + '**/*.js' )
        .pipe( $.jshint() )
        .pipe( $.jshint.reporter('jshint-stylish') );
};

/**
 * Implement lazypipe to repeat the following tasks on our JavaScript files
 *   1. Combine all our source files
 *   2. Add our banner
 *   3. Minify the files
 * @type {[type]}
 */
var jsTasks = $.lazypipe()
        .pipe( $.header, banner.main, {pkg: pkg} )              // Add our banner
        .pipe( gulp.dest, pkg.paths.dist.js )                   // Write the pretty files
        .pipe( $.rename, {suffix: '.min' } )                    // Add .min
        .pipe( $.uglify )                                       // Make 'em ugly
        .pipe( gulp.dest, pkg.paths.dist.js );                  // Output it

/**
 * Handle Javascript
 *
 * This function does the following:
 *   1. Lints all the scripts we wrote
 *   2. Runs all the tasks jsTasks
 *   3. Signals async completion
 */
function scripts( cb ) {
    lintScripts();                                              // Lint all the scripts we wrote

    var frontend = pkg.paths.src.js.appLibraries;
    if ( frontend.length > 0 ) {
        frontend.push( pkg.paths.src.js.fe + '**/*.js' );
    } else {
        frontend = pkg.paths.src.js.fe + '**/*.js';
    }

    var admin = pkg.paths.src.js.adminLibraries;
    if ( admin.length > 0 ) {
        admin.push( pkg.paths.src.js.admin + '**/*.js' );
    } else {
        admin = pkg.paths.src.js.admin + '**/*.js';
    }

    var runFrontend = gulp                                        // Process frontend scripts
        .src( frontend )                                          // Front end source
        .pipe( $.babel({
            presets: ['@babel/env']
        }))
        .pipe( $.concat( pkg.name + '.js' ) )                     // Concat the libraries and our source files
        .pipe( jsTasks() )                                        // Runs all our tasks
        .pipe( $.noop() );                                        // noop() because lazypipe sucks and doesn't signal completion

    var runAdmin = gulp
        .src( admin )
        .pipe( $.babel({
            presets: ['@babel/env']
        }))
        .pipe( $.concat( pkg.name + '-admin.js' ) )
        .pipe( jsTasks() )
        .pipe( $.noop() );

    return $.mergeStream( runFrontend, runAdmin );
};

/**
 * Compress our Images
 *
 * Note: If you are creating an SVG sprite, do not place SVGs in your image directory
 */
function images( cb ) {
    return gulp
        .src( pkg.paths.src.img + "**/*.{png,jpg,jpeg,gif,svg}" )
        .pipe( $.imagemin({
            progressive: true,
            interlaced: true,
            optimizationLevel: 3,
            svgoPlugins: [{removeViewBox: false}],
            verbose: true,
            use: []
        }))
        .pipe( gulp.dest( pkg.paths.dist.img ) );
};

/**
 * Xerox
 *
 * This function copies all folders and files in ./src/copy directory and places
 * them in the ./build directory
 */
function copyFiles( cb ) {
    return gulp
        .src( pkg.paths.copy.input + '**/*.*' )
        .pipe( gulp.dest( pkg.paths.copy.output ) );
    cb();
};

// Handle SVG Sprite creation
function svg( cb ) {
    return gulp
        .src( pkg.paths.src.svg + '**/*.svg' )
        .pipe( $.svgmin( function(file) {
            var prefix = $.path.basename( file.relative, $.path.extname(file.relative) );
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe( $.svgstore({inlineSvg: true}) )
        .pipe( $.cheerio( function($){
            $('svg').attr( 'style', 'display: none' )
        }))
        .pipe( $.rename( pkg.name + '.svg' ))
        .pipe( gulp.dest( pkg.paths.dist.svg ) );
};

// Watch all the things
function watchFiles( cb ) {
    gulp.watch( pkg.paths.src.sass, styles );
    gulp.watch( pkg.paths.src.js.base, gulp.series( scripts, reload ) );
    gulp.watch( pkg.paths.src.img, gulp.series( images, reload ) );
    gulp.watch( pkg.paths.src.svg, gulp.series( svg, reload ) );
    gulp.watch( pkg.paths.src.php, reload);
    gulp.watch( pkg.paths.copy.input, gulp.series( copyFiles, reload ) );
    cb();
};

/**
 * Zips theme or plugin and places in the parent directory
 *
 * All credit goes to @AhmadAwais and WPGulp
 */
function zip( cb ) {
    var include = ['./**/*'];
    var exclude = [
        '!./{node_modules,node_modules/**/*}',
        '!./.git',
        '!./.gitignore',
        '!./gulpfile.js',
        '!./package-lock.json',
        '!./src',
        '!./src/**/*',
        '!./vendor_libraries',
        '!./vendor_libraries/**/*'
    ];
    var src = [...include, ...exclude];

    return gulp
        .src( src )
        .pipe( $.zip( pkg.name + '.zip' ) )
        .pipe( gulp.dest( './../' ) );
};

exports.compile = gulp.series(
    clean,
    linkDependencies,
    gulp.parallel(
        styles,
        scripts,
        images,
        svg,
        copyFiles
    )
);

exports.default = gulp.series(
    exports.compile,
    startServer,
    watchFiles
);

exports.watch = gulp.series(
    startServer,
    watchFiles
);

exports.styles = styles;

exports.scripts = scripts;

exports.svg = svg;

exports.images = images;

exports.copyfiles = copyFiles;

exports.clean = clean;

exports.zip = zip;