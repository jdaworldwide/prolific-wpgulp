# Prerequisites
This project uses Gulp, a [Node.js](https://nodejs.org/en/download/package-manager/) task runner and should already be installed as a prerequisite.

Confirm that node and npm are properly installed using the command line:

    node --version
    npm --version

Additionally, the Gulp command command line must be globaly installed.

    npm install --global gulp-cli

You can verify that gulp-cli is installed by running

    gulp --version

Finally, portions of this project are built using [Composer](https://getcomposer.org), and `npm postinstall` will fail if Composer is not installed. Verify composer is properly installed with:

    composer --version

# Quickstart: Homebrew on macOS
On macOS, ensure command line tools are installed. If they are not, install them using

    xcode-select --install


Then, install and update [Homebrew](https://brew.sh).
At the time of this writing, past the following into terminal:

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew doctor
    brew upgrade

Next, install node, gulp-cli and composer:

    brew install node gulp-cli composer

# About This Project

## Overview
This is a WordPress starter theme and Gulp workflow designed for use with new Prolific / JDA Worldwide web projects. If this is your first time building this project, run `npm install` to pull down dependencies. This command also includes an npm `postinstall` script to install our composer libraries in the `inc` directory. After dependencies, your **first** task should be to customize the `package.json` file included with this project.

## Changes to wp-config.php
If using MAMP, set your `DB_HOST` to `127.0.0.1` to enable `wp-cli` functionality. Additionally, make sure that `Allow network access to MySQL ` is checked in MAMP>MySQL Settings.

To properly set up debugging, add these lines to `wp-config.php`:

    define( 'WP_DEBUG', true );
    define( 'WP_DEBUG_LOG', true );
    define( 'WP_DEBUG_DISPLAY', false );
    define( 'WP_POST_REVISIONS', 10 );

## The package.json file explained ...
The entire build process relies upon values stored in this projects `package.json` file. Below is an explanation of all the attributes and what should be changed for each attribute

### tl;dr ...
Upate the following attributes: `name, theme-name, version, author, copyright, url's, and dependencies`.

### Basic Information
At various parts of our build process and throughout our theme library, both of these values are referenced frequently. If this project is named **Prolific Starter Theme**, these values should look something like this:

    {
        "name": "prolific-starter-theme",
        "theme-name": "Prolific Starter Theme",
        "version": "0.0.1",
        "author": "Brandon Travis, Prolific <btravis@prolific.world>",
        "copyright": "Prolific"
    }

Note: The version is relatively arbitrary, however should follow [Semantic Versioning](https://semver.org/) guidelines (Major.Minor.Patch). The version number is used when enqueuing assets for cache busting purposes. The author name and copyright are relatively arbitraty however the values should exist to properly write file headers during gulp compilation.

### URLs
At this time, the `local` property is the only one in use. This should match a URL being served by something like MAMP. During our `gulp watch` process, Browser Sync will create a proxy to this URL. For live preview to work, you **must** configure this value.

### Paths
In general, the existing values should not need to be edited with two exceptions:

    {
        "paths": {
            "src": {
                "js": {
                    "appLibraries": [
                        "./node_modules/slick-carousel/slick/slick.js",
                        "./node_modules/path/to/dependence/js"
                    ]
                },
                "adminLibraries": []
            }
        }
    }

If you intend to install JavaScript dependencies using NPM, adding the absolute path to the JavaScript file will include the file in our final build.

### Dependencies
The entire gulp workflow depends on the modules listed under `path.devDependencies`. Additional dependencies can be installed using npm:

    npm install -D package-name

Removing existing dependencies will very likely break Gulp.

To add a new frontend dependency, use NPM

    npm install -S package-name

Note: During our build process, a shadow folder named `vendor_libraries` is created with symlinks to our frontend dependencies. This makes finding necessary js/css files significantly easier, but you can also simply browse through `node_modules` if you would prefer.

# What does our taskrunner do?

## CSS
Gulp compiles the five core scss files found in `src/sass` and outputs them to the `build` directory.

**style.scss:** Our main stylesheet handling all the styles for the frontend of our theme.
**admin.scss:** Styles that get loaded into /wp-admin. Note: These are **not** editor / gutenberg styles.
**editor.scss:** These styles get loaded whenever TinyMCE is rendered on the backend. This can include inside of Gutenberg / ACF blocks that use TinyMCE.
**blocks.scss:** These styles are used by wp-admin to render ACF / Gutenberg blocks on the backend.
**print.scss:** Basic print styles.

Gulp will:
1. Watch for any changes to `src/sass`,
2. Create human-readable CSS files,
3. Auto-prefix any properties necessary as defined in the `package.json` file,
4. Prepend our banner,
5. Write sourcemaps as `*.map` files,
6. Create LTR-specific files,
7. Minify our files,
8. Place all files in our `build/css` directory
9. Inject these new files into our BrowserSync session

## JavaScript
Gulp compiles the `admin.js` and `app.js` files and related modules.

First, ensure that any dependencies have been defined in the `package.json` file and that the path to these files has been included (See Paths above).

Gulp will:
1. Lint scripts that **we** wrote,
2. Concatenate necessary files,
3. Prepend our banner,
4. Write human-readable files,
5. Uglify our files
6. Write pretty and ugly files to `build/js` as `theme-name.js` and `theme-name-admin.js` and their `.min` counterparts
7. Reload browsers

## Images
Gulp will watch the `src/img` folder for png, jpg, jpeg, gif, and svg files placed in this directory. When detected, it will attempt to minify the files and place them in `build/img`. These should be for images related to our UI / Theme specific, and not images utilized on the frontend or in the CMS portions of the website.

## SVG
Gulp will watch for SVG files placed in the `src/svg` directory. This can be used to create an icon system or when working with reusable SVGs. This file is added in the `header.php` file.

1. Concatenate all svg's,
2. strip out th xml headers,
3. create `<symbol></symbol>` elements based on file name,
4. Add `display: none` to the SVG element,
5. Place the file in `build/svg`

## Copy
Gulp will copy everything in the `src/copy` folder and output it directly to the `build` folder. For example, if you have files in a folder located at `src/copy/fonts/../*.*`, gulp would move all of those files to `build/fonts`

## Zip
To zip the theme, run `gulp zip`

# Theme Variables
In functions.php, there are a number of variables that can be defined to change theme behavior. They are self-explanatory.

# Managing browser support
Browser support (autoprefixer) is defined in the package.json file, and should be kept current with [WordPress Core recommendations](https://make.wordpress.org/core/handbook/best-practices/browser-support).