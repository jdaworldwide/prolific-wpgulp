<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */
?>
        <footer></footer>
    </div> <!-- end .site-container -->
    <?php wp_footer(); ?>
</body>
</html>