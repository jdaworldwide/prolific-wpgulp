<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

get_header();

if ( have_posts() ) :
    echo '<main class="flexible-content">';
    while( have_posts() ) : the_post();
        echo '<article>';
            echo '<h1>' . get_the_title() . '</h1>';
            echo '<div>' . apply_filters( 'the_content', get_the_content() ) . '</div>';
        echo '</article>';
    endwhile;
    echo '</main>';
endif;
get_footer();
