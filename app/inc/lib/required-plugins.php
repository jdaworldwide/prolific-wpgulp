<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

require_once( INC_DIR . '/vendor/tgmpa/tgm-plugin-activation/class-tgm-plugin-activation.php' );

add_action( 'tgmpa_register', 'pro_required_plugins' );
function pro_required_plugins() {
    $dir = get_stylesheet_directory() . '/inc/lib/required-plugins/';

    $plugins = array(
        array(
            'name' => 'Advanced Custom Fields Pro',
            'slug' => 'advanced-custom-fields-pro',
            'source' => $dir . 'acf.zip',
            'required' => true,
            'force_activation' => true,
            'force_deactivation' => false
        ),
        array(
            'name' => 'ACF Extended Pro',
            'slug' => 'acf-extended-pro',
            'source' => $dir . 'acf-extended-pro.zip',
            'required' => true,
            'force_activation' => true,
            'force_deactivation' => false
        ),
        array(
            'name' => 'User Switching',
            'slug' => 'user-switching',
            'source' => $dir . 'user-switching.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),
        array(
            'name' => 'Gravity Forms',
            'slug' => 'gravityforms',
            'source' => $dir . 'gravityforms.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),
        array(
            'name' => 'WP Migrate DB Pro',
            'slug' => 'wp-migrate-db-pro',
            'source' => $dir . 'wp-migrate-db-pro.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),
        array(
            'name' => 'WP Migrate DB Pro Media Files',
            'slug' => 'wp-migrate-db-pro-media-files',
            'source' => $dir . 'wp-migrate-db-pro-media-files.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),
        array(
            'name' => 'What the File',
            'slug' => 'what-the-file',
            'source' => $dir . 'what-the-file.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),
        array(
            'name' => 'Yoast Duplicate Post',
            'slug' => 'duplicate-post',
            'source' => $dir . 'duplicate-post.zip',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false
        )
    );

    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
    );
    tgmpa( $plugins, $config );
}