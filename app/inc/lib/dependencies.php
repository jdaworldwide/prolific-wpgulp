<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

add_action( 'after_setup_theme', 'pro_setup_frontend_dependencies' );
function pro_setup_frontend_dependencies() {
    if ( ! is_admin() ) {
        add_action( 'wp_enqueue_scripts', 'pro_enqueue_front_end_styles' );
        add_action( 'wp_enqueue_scripts', 'pro_enqueue_print_styles' );
        add_action( 'wp_enqueue_scripts', 'pro_enqueue_front_end_scripts' );
    }
}

add_action( 'after_setup_theme', 'pro_setup_admin_dependencies' );
function pro_setup_admin_dependencies() {
    if ( is_admin() ) {
        add_action( 'admin_enqueue_scripts', 'pro_enqueue_admin_styles' );
        add_action( 'admin_enqueue_scripts', 'pro_enqueue_admin_scripts' );
        add_action( 'enqueue_block_editor_assets', 'pro_enqueue_block_styles' );
        add_action( 'enqueue_block_editor_assets', 'pro_enqueue_block_scripts' );
    }
}

function pro_enqueue_front_end_styles() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG;

    $style = CSS_URL . '/style' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() );

    wp_register_style( $handle, $style, false, $ver, 'screen' );
    wp_enqueue_style( $handle );
}

function pro_enqueue_print_styles() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG . '-print';
    $print = CSS_URL . '/print' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() );

    wp_register_style( $handle, $print, false, $ver, 'print' );
    wp_enqueue_style( $handle );
}

function pro_enqueue_front_end_scripts() {

    // For performance reasons, load jQuery from Google's CDN
    // If the theme has defined a specific version of jQuery, use that version.
    // Otherwise, check for the jquery version defined by WordPress and use that
    // version. If all else fails, manually select version 1.12.4.
    // Version 1.12.4 is the default version of jQuery used by WordPress, however
    // this is due to be updated in 5.5. Once this happens, we should update our
    // minimum version here.

    if ( defined( 'JQUERY_VERSION' ) ) {
        $jver = JQUERY_VERSION;
    } else {
        global $wp_scripts;

        if ( isset( $wp_scripts->registered['jquery']->ver ) ) {
            $jver = $wp_scripts->registered['jquery']->ver;
            $jver = str_replace('-wp', '', $jver);
        } else {
            $jver = '1.12.4';
        }
    }

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jver . '/jquery.min.js', array(), $jver, true );
    wp_enqueue_script( 'jquery' );

    $ver = THEME_VERSION;
    $handle = THEME_SLUG;

    $js = JS_URL . '/' . $handle . ( ! pro_is_dev() ? '.min.js' : '.js?v=' . time() );

    wp_register_script( $handle, $js, array( 'jquery' ), $ver, true );
    wp_enqueue_script( $handle );

    // Best I can tell, this is completely unnecssary in current versions of
    // WordPress. Kill it so we have one less http request.
    wp_deregister_script( 'wp-embed' );
    wp_register_script( 'wp-embed', null, array( $handle ), null, true );
}

function pro_enqueue_admin_styles() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG . '-admin';

    $style = CSS_URL . '/admin' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() );

    wp_register_style( $handle, $style, false, $ver, 'screen' );
    wp_enqueue_style( $handle );

    // add_editor_style( CSS_URL . '/editor' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() ) );
}

function pro_enqueue_admin_scripts() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG . '-admin';

    $js = JS_URL . '/' . $handle . ( ! pro_is_dev() ? '.min.js' : '.js?v=' . time() );

    wp_register_script( $handle, $js, array( 'jquery' ), $ver, false );
    wp_enqueue_script( $handle );
}

function pro_enqueue_block_styles() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG . '-block-editor-styles';
    $block = CSS_URL . '/blocks' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() );

    wp_register_style( $handle, $block, false, $ver, 'screen' );
    wp_enqueue_style( $handle );

    wp_enqueue_style( $handle . '-editor', CSS_URL . '/editor' . ( pro_is_dev() ? '.min.css' : '.css?v=' . time() ), false, '1.0', 'all' );
}

function pro_enqueue_block_scripts() {
    $ver = THEME_VERSION;
    $handle = THEME_SLUG . '-block-editor-js';

    $js = JS_URL . '/' . THEME_SLUG . ( ! pro_is_dev() ? '.min.js' : '.js?v=' . time() );

    wp_register_script( $handle, $js, array( 'jquery' ), $ver, true );
    wp_enqueue_script( $handle );
}