<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

function pro_debug( $content, $bool = false ) {

    if ( $bool ) {
        echo '<pre>' . $content . '</pre>';
    } else {
        echo '<pre>';
        print_r( $content );
        echo '</pre>';

    }
}

function pro_debug_log( $log )  {
  if ( is_array( $log ) || is_object( $log ) ) {
     error_log( print_r( $log, true ) );
  } else {
     error_log( $log );
  }
}

/**
 * Common sense function for getting automating image retrieval.
 *
 * Added srcset from Dan and updated caption to use wp_get_attachment_caption
 */
function pro_get_attachment( $attachment_id, $size = '' ) {
    $attachment = get_post( $attachment_id );

    if ( ! $attachment )
        return;

    $src = ( $size != '' ) ? wp_get_attachment_image_src( $attachment_id, $size )[0] : wp_get_attachment_url($attachment_id);
    $srcset = wp_get_attachment_image_srcset( $attachment_id );

    return array(
        'id' => $attachment_id,
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => wp_get_attachment_caption( $attachment->ID ),
        'href' => get_permalink( $attachment->ID ),
        'src' => $src,
        'title' => $attachment->post_title,
        'name' => $attachment->post_name,
        'description' => $attachment->post_content
    );
}

function pro_slugify($string, $replace = array(), $delimiter = '-') {

    $clean = $string;
    if (!empty($replace)) {
        $clean = str_replace((array) $replace, ' ', $clean);
    }
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);

    return $clean;
}

/**
 * This function accepts a page ID and returns true if you are either on that
 * page OR any of its sub-pages. How WordPress still doesn't have this is beyond
 * me.
 */
function pro_is_tree($pid) {
    //* From https://css-tricks.com/snippets/wordpress/if-page-is-parent-or-child/
    global $post;

    if ( is_page() && $post->post_parent == $pid || is_page($pid) ) {
        return true;
    } else {
        return false;
    }
}

/**
 * Check if the root page of the site is being viewed.
 *
 * `is_front_page()` returns false for the root page of a website when
 * - the WordPress "Front page displays" setting is set to "A static page"
 * - "Front page" is left undefined
 * - "Posts page" is assigned to an existing page
 *
 * This function checks for is_front_page() or the root page of the website
 * in this edge case.
 *
 * @since 0.1.0
 */
function pro_is_root_page() {

    return is_front_page() || ( is_home() && get_option( 'page_for_posts' ) && ! get_option( 'page_on_front' ) && ! get_queried_object() );
}

function pro_code( $content ) {

    return '<code>' . esc_html( $content ) . '</code>';
}

/**
 * Calculate the time difference - a replacement for `human_time_diff()` until it is improved.
 *
 * Based on BuddyPress function `bp_core_time_since()`, which in turn is based on functions created by
 * Dunstan Orchard - http://1976design.com
 *
 * This function will return an text representation of the time elapsed since a
 * given date, giving the two largest units e.g.:
 *
 *  - 2 hours and 50 minutes
 *  - 4 days
 *  - 4 weeks and 6 days
 *
 * @since  0.1.0
 */
function pro_human_time_diff( $older_date, $newer_date = false, $relative_depth = 2 ) {

    // If no newer date is given, assume now.
    $newer_date = $newer_date ? $newer_date : time();

    // Difference in seconds.
    $since = absint( $newer_date - $older_date );

    if ( ! $since ) {
        return '0 ' . _x( 'seconds', 'time difference', 'genesis' );
    }

    // Hold units of time in seconds, and their pluralised strings (not translated yet).
    $units = array(
        array( 31536000, '%s year', '%s years', 'time difference' ),  // 60 * 60 * 24 * 365
        array( 2592000, '%s month', '%s months', 'time difference' ), // 60 * 60 * 24 * 30
        array( 604800, '%s week', '%s weeks', 'time difference' ),    // 60 * 60 * 24 * 7
        array( 86400, '%s day', '%s days', 'time difference' ),       // 60 * 60 * 24
        array( 3600, '%s hour', '%s hours', 'time difference' ),      // 60 * 60
        array( 60, '%s minute', '%s minutes', 'time difference' ),
        array( 1, '%s second', '%s seconds', 'time difference' ),
    );

    // Build output with as many units as specified in $relative_depth.
    $relative_depth = (int) $relative_depth ? (int) $relative_depth : 2;
    $i = 0;
    $counted_seconds = 0;
    $date_partials = array();
    while ( count( $date_partials ) < $relative_depth && $i < count( $units ) ) {
        $seconds = $units[$i][0];
        if ( ( $count = floor( ( $since - $counted_seconds ) / $seconds ) ) != 0 ) {
            $date_partials[] = sprintf( $units[$i][1], $count );
            $counted_seconds += $count * $seconds;
        }
        $i++;
    }

    if ( empty( $date_partials ) ) {
        $output = '';
    } elseif ( 1 == count( $date_partials ) ) {
        $output = $date_partials[0];
    } else {

        // Combine all but last partial using commas.
        $output = implode( ', ', array_slice( $date_partials, 0, -1 ) );

        // Add 'and' separator.
        // $output .= ' ' . _x( 'and', 'separator in time difference', 'genesis' ) . ' ';
        $output .= ' and ';

        // Add last partial.
        $output .= end( $date_partials );
    }

    return $output;
}

function pro_is_dev() {
    if ( $_SERVER["REMOTE_ADDR"] == '127.0.0.1' || $_SERVER["REMOTE_ADDR"] == '::1' ) {
        return true;
    } else {
        return false;
    }
}

function pro_posts_nav_numeric() {

    if ( is_singular() )
        return;

    global $wp_query;

    //* Stop execution if there is only one page
    if ( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    //* Add current page to the array
    if ( $paged >= 1 );
        $links[] = $paged;

    //* Add the pages around the current page to the array
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<nav class="archive-pagination">';

    $before_number = '<span class="screen-reader-text">Page </span>';

    echo '<ul>';

    $previous_label = '<i class="fa fa-lg fa-angle-left"></i><span class="screen-reader-text">Previous</span>';
    $next_label     = '<span class="screen-reader-text">Next</span><i class="fa fa-lg fa-angle-right"></i>';

    //* Previous post link
    if ( get_previous_posts_link() )
        printf( '<li class="pagination-previous">%s</li>' . "\n", get_previous_posts_link( $previous_label ) );

    //* Link to first page, plus ellipses if necessary
    if ( ! in_array( 1, $links ) ) {

        $class = 1 == $paged ? ' class="pagination-active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), $before_number . '1' );

        if ( ! in_array( 2, $links ) ) {
            echo '<li class="pagination-omission">&#x02026;</li>' . "\n";
        }

    }

    //* Link to current page, plus 2 pages in either direction if necessary
    sort ( $links );
    foreach ( (array) $links as $link ) {

        $class = $paged == $link ? ' class="pagination-active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $before_number . $link );

    }

    //* Link to last page, plus ellipses if necessary
    if ( ! in_array ( $max, $links ) ) {

        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="pagination-omission">&#x02026;</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $before_number . $max );

    }

    //* Mext Post Link

    if ( get_next_posts_link() )
        echo '<li class="pagination-next">', get_next_posts_link( $next_label ), '</li>';

    echo '</ul></nav>' . "\n";
}

function pro_do_breadcrumbs() {

  $posts_page_id = get_option( 'page_for_posts' );

    $o  = '<ul class="breadcrumbs">';
    $o .= '<li><a href="' . esc_url( home_url( '/' ) ) . '">Home</a></li>';


    if ( is_home() ) {
        // The main Blog page
        $o .= '<li>' . get_the_title( get_option('page_for_posts', true) ) . '</li>';
        $o .= '</ul>';

        return $o;
    } elseif ( is_category() || is_tag() ) {
        $o .= '<li><a href="' . home_url(get_page_uri($posts_page_id)) . '">' . get_the_title( get_option('page_for_posts', true) ) . '</a></li>';

        if ( is_category() ) {
            $o .= '<li>' . single_cat_title('', false) . '</li>';
        } elseif( is_tag() ) {
            $o .= '<li>' . single_tag_title('', false) . '</li>';
        }

        $o .= '</ul>';

        return $o;
    } elseif ( is_singular() ) {
        global $post;

        // Post
        if ( is_singular('post') ) {
            $o .= '<li><a href="' . home_url(get_page_uri($posts_page_id)) . '">' . get_the_title( get_option('page_for_posts', true) ) . '</a></li>';
            $cats = get_the_category();

            // If the post has a category
            if ( $cats[0] ) {

                // if the category isn't Uncategorized
                if ( $cats[0]->term_id != '1' ) {
                     $o .= '<li><a href="' . esc_url( get_category_link($cats[0]->term_id) ) . '">' . $cats[0]->name . '</a></li>';
                }
                $o .= '<li>' . get_the_title() . '</li>';
            }
        } elseif ( is_singular('page') ) {

            // Page has a parent
            if ( $post->post_parent ) {

                // Get parents
                $anc = get_post_ancestors( $post->ID );

                // Get them in the right order
                $anc = array_reverse( $anc );

                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li><a href="' . get_permalink($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }

                $o .= $parents;

                $o .= '<li>' . get_the_title() . '</li>';

            } else { // page has no parent
                $o .= '<li>' . get_the_title() . '</li>';
            }
        } elseif ( is_singular('career') ) {
            $careers_page = get_page_by_title( 'careers' );

            if ( $careers_page ) {
                $o .= '<li><a href="' . get_permalink($careers_page) . '">' . get_the_title($careers_page) . '</a></li>';
            }
            $o .= '<li>' . get_the_title() . '</li>';
        }

        $o .= '</ul>';
    }  elseif( is_search() ) {
        $o .= '<li>Search Results</li>';
        $o .= '</ul>';
    } elseif ( is_404() ) {
        $o .= '<li>404 - Page Not Found</li>';
        $o .= '</ul>';
    }
    return $o;
}

//* Dump in use gutenberg blocks. Used for debugging
function pro_dump_blocks( $log = false ) {
    global $post;
    if ( $log == true ) {
        return pro_debug_log( esc_html( $post->post_content ) );
    } else {
        return pro_debug( esc_html( $post->post_content ) );
    }
}

/**
 * Sort a multi-domensional array of objects by key value
 * Usage: usort($array, arrSortObjsByKey('VALUE_TO_SORT_BY'));
 * Expects an array of objects.
 *
 * @param String    $key  The name of the parameter to sort by
 * @param String    $order the sort order
 * @return A function to compare using usort
 */
function pro_arr_sort_by_objs_key($key, $order = 'ASC') {
    return function($a, $b) use ($key, $order) {

        // Swap order if necessary
        if ($order == 'DESC') {
            list($a, $b) = array($b, $a);
        }

        // Check data type
        if (is_numeric($a->$key)) {
            return $a->$key - $b->$key; // compare numeric
        } else {
            return strnatcasecmp($a->$key, $b->$key); // compare string
        }
    };
}