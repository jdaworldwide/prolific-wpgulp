<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

// Move Gravity Forms to footer to fix jQuery issues. Why on earth do so many
// vendors STILL load their stuff in the header?
add_filter("gform_init_scripts_footer", "__return_true");
add_filter( 'gform_cdata_open', 'pro_wrap_gform_cdata_open' );
add_filter( 'gform_cdata_close', 'pro_wrap_gform_cdata_close' );

function pro_wrap_gform_cdata_open( $content = '' ) {
    if ( ! pro_do_wrap_gform_cdata() ) {
        return $content;
    }
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ' . $content;
    return $content;
}

function pro_wrap_gform_cdata_close( $content = '' ) {
    if ( ! pro_do_wrap_gform_cdata() ) {
        return $content;
    }
    $content .= ' }, false );';
    return $content;
}

function pro_do_wrap_gform_cdata() {
    if (
        is_admin()
        || ( defined( 'DOING_AJAX' ) && DOING_AJAX )
        || isset( $_POST['gform_ajax'] )
        || isset( $_GET['gf_page'] ) // Admin page (eg. form preview).
        || doing_action( 'wp_footer' )
        || did_action( 'wp_footer' )
    ) {
        return false;
    }
    return true;
}

add_filter( 'gform_ajax_spinner_url', 'pro_gf_spinner_replace', 10, 2 );
function pro_gf_spinner_replace( $image_src, $form ) {
    return  'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'; // relative to you theme images folder
}