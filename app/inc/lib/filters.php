<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

add_action('init', 'pro_use_gutenburg');
function pro_use_gutenburg() {

    if ( defined('USE_GUTENBURG') ) {
        if ( USE_GUTENBURG == false ) {
            add_filter('use_block_editor_for_post', '__return_false', 10);
        }
    }
}

/**
 * Allow more upload file types.
 */
add_filter('upload_mimes', 'pro_mime_types', 1, 1);
function pro_mime_types( $mime_types) {
    $mime_types['svg'] = 'image/svg+xml';
    $mime_types['ai'] = 'application/postscript';
    $mime_types['eps'] = 'application/postscript';
    $mime_types['sketch'] = 'application/octet-stream';
    $mime_types['indd'] = 'application/octet-stream';
    $mime_types['epub'] = 'application/epub+zip';
    $mime_types['mobi'] = 'application/x-mobipocket-ebook';
    return $mime_types;
}

add_filter( 'excerpt_length', 'pro_excerpt_length' );
function pro_excerpt_length( $text ) {
    // If theme variable EXCERPT_LENGTH exists, use its value to trim the
    // excerpt, otherwise use the default of 55
    $length = ( defined( 'EXCERPT_LENGTH' ) ) ? EXCERPT_LENGTH : 55 ;

    return $length;
}

add_filter( 'excerpt_more', 'pro_excerpt_more' );
function pro_excerpt_more( $text ) {
    $more = ( defined( 'EXCERPT_MORE' ) ) ? EXCERPT_MORE : ' [&hellip;]';

    return $more;
}

/**
 * Add `entry` post class, remove `hentry` post class to satisfy Google's structured data requirements.
 *
 * @since  0.1.0
 */
add_filter( 'post_class', 'pro_entry_post_class' );
function pro_entry_post_class( $classes ) {

    //* https://search.google.com/structured-data/testing-tool/u/0/
    //* https://swampsidestudio.com/remove-wordpress-hentry-class/

    if ( ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) && is_admin() ) {
        return $classes;
    }

    //* Add "entrty" to post class array.
    $classes[] = 'entry';

    // Remove "hentry" from post class array
    $classes = array_diff( $classes, array( 'hentry' ) );

    return $classes;
}