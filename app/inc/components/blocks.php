<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

add_action( 'acf/init', 'pro_load_blocks' );
function pro_load_blocks() {
    if ( function_exists( 'acf_register_block_type' ) ) {
        acf_register_block_type( array(
            'name'            => 'wysiwyg-editor',
            'title'           => __('WYSIWYG Editor'),
            'description'     => __('A custom block with a traditional WYSIWYG editor.' ),
            'render_template' => 'template-parts/blocks/wysiwyg_editor.php',
            'category'        => sanitize_title_with_dashes( THEME_NAME ) . '-blocks',
            'icon'            => 'analytics',
            'keywords'        => array( 'text editor', 'tinymce', 'wysiwyg' ),
            'enqueue_style'   => CSS_URL .'/blocks.min.css'
        ));
    }
}

add_filter( 'block_categories', 'pro_block_categories', 10, 2);
function pro_block_categories( $categories, $post ) {
  return array_merge(
    array(
      array(
        'slug' => sanitize_title_with_dashes( THEME_NAME ) . '-blocks',
        'title' => __( THEME_NAME . ' Blocks', sanitize_title_with_dashes( THEME_NAME ) . '-blocks' ),
      ),
    ),
    $categories
  );
}
