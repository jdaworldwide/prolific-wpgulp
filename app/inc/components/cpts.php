<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

require_once( INC_DIR . '/vendor/johnbillion/extended-cpts/extended-cpts.php' );

add_action( 'init', 'pro_load_custom_post_types' );
function pro_load_custom_post_types() {
    register_extended_post_type( 'sample' );
}

add_action( 'init', 'pro_load_custom_taxonomies' );
function pro_load_custom_taxonomies() {
    register_extended_taxonomy( 'location', 'sample' );
}