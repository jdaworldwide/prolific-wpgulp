<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

// Relocate debug log to theme directory.
if (defined('WP_DEBUG_LOG') && WP_DEBUG_LOG) {
    ini_set( 'error_log', get_stylesheet_directory() . '/debug.log' );
}

add_action( 'pro_init', 'pro_setup_theme_support' );
function pro_setup_theme_support() {
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'custom-logo' );
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
    add_theme_support( 'title-tag' );
    add_theme_support( 'responsive-embeds' ); // This is specifically for embeds created by Gutenberg
}

add_action( 'pro_init', 'pro_theme_constants' );
function pro_theme_constants() {
    $pkg = file_get_contents( get_stylesheet_directory() .'/package.json' );
    $pkg = json_decode( $pkg, true );
    define( 'THEME_NAME', $pkg['theme-name'] );
    define( 'THEME_VERSION', $pkg['version'] );
    define( 'THEME_SLUG', sanitize_title_with_dashes( THEME_NAME ) );

    define( 'THEME_DIR', get_stylesheet_directory() );
    define( 'BUILD_DIR', THEME_DIR . '/build' );
    define( 'INC_DIR', THEME_DIR . '/inc' );
    define( 'CSS_DIR', BUILD_DIR . '/css' );
    define( 'JS_DIR', BUILD_DIR . '/js' );
    define( 'SVG_DIR', BUILD_DIR . '/svg' );

    define( 'THEME_URL', get_stylesheet_directory_uri() );
    define( 'BUILD_URL', THEME_URL . '/build' );
    define( 'INC_URL', THEME_URL . '/inc' );
    define( 'CSS_URL', BUILD_URL . '/css' );
    define( 'JS_URL', BUILD_URL . '/js' );
    define( 'SVG_URL', BUILD_URL . '/svg' );
}

add_action( 'pro_init', 'pro_load_framework' );
function pro_load_framework() {
    $lib_dir = trailingslashit( INC_DIR ) . 'lib/';

    require_once( $lib_dir . 'filters.php' );
    require_once( $lib_dir . 'helpers.php' );
    require_once( $lib_dir . 'dependencies.php' );
    require_once( $lib_dir . 'fix-gravity-forms.php' );
    require_once( $lib_dir . 'required-plugins.php' );

    //* Maybe disable comments
    if ( defined( 'DISABLE_COMMENTS' ) ) {
        if ( DISABLE_COMMENTS == true ) {
            require_once( $lib_dir . 'disable-comments.php' );
        }
    }
}
do_action( 'pro_init' );