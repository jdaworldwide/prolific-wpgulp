<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

/**
 * WYSIWYG Editor Block Template.
 */

// Create id attribute allowing for custom "anchor" value.
$block_id = 'block-' . $block['id'];

// Load values and assign defaults.
$content = get_field('content');

?>
<section class="wysiwyg-editor" id="<?php echo $block_id; ?>">

    <div class="wysiwyg-editor__content">
        <?php echo $content; ?>
    </div>

</section>