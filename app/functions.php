<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

//* Setup theme framework
define( 'USE_GUTENBURG', true );
define( 'EXCERPT_LENGTH', 200 );
define( 'EXCERPT_MORE', ' &hellip;' );
define( 'DISABLE_COMMENTS', true );
define( 'JQUERY_VERSION', '3.5.1' );

//* Load Theme Library
require_once( dirname( __FILE__ ) . '/inc/init.php' );

//* Load custom post types and taxonomies
require_once( dirname( __FILE__ ) . '/inc/components/cpts.php' );

//* Load ACF Blocks
require_once( dirname( __FILE__ ) . '/inc/components/blocks.php' );