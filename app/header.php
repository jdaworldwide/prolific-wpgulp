<?php
/**
 * Prolific Starter Theme
 *
 * @package Prolific
 * @subpackage Prolific_2020
 * @since Prolific 0.0.1
 */

$pkg = file_get_contents( get_stylesheet_directory() .'/package.json' );
$pkg = json_decode( $pkg, true );

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php echo body_class(); ?>>
    <?php if ( defined( 'SVG_DIR' ) && defined( 'THEME_SLUG' ) ) :
        if ( file_exists( SVG_DIR . '/' . THEME_SLUG . '.svg' ) ) : include_once( SVG_DIR . '/' . THEME_SLUG . '.svg' ); endif;
    endif; ?>
    <div class="site-container" id="top">
        <header></header>